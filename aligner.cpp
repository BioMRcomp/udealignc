/*
 * aligner.cpp
 *
 *  Created on: Apr 25, 2017
 *      Author: jlotero
 */
 
#define IMPLEMENT_CUDA
//#define IMPLEMENT_FPGA

#include "aligner.h"
#include "transform.hpp"
#define MAX_LINE_SIZE 200
#define TEMP_FILE "temp_file"

//#define SHOWCOMP			//Display a direct comparison between read and match in reference
//#define SHOWLOC			//Show location of read within reference
//#define SHOWALIGN			//Print solution vectors (which generate SAM fields)
//#define SHOWMISS

#ifdef IMPLEMENT_CUDA
#include "extendCUDA.cuh"
#define A_VALUE 0b00
#define C_VALUE 0b01
#define T_VALUE 0b10
#define G_VALUE 0b11
#define N_VALUE 0b00

#elif defined IMPLEMENT_FPGA
#include "serial.h"
#include "extend.h"
#else
#include "extend.h"
#endif

//#define SHOWLOC
char* Read::reference = NULL;

ifstream::pos_type filesize(const char* filename)
{
    ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg();
}

Read::Read(char *seq, int len){
      length = len;
      besthit = 0;
      id = 0;
      orientation = 'F';
      score = 0;
      mapped = false;
      sequence = seq;
      seed_pos = 0;
      hits = 0;
      CIGAR_length = 0;
      MD_length = 0;
      CIGAR = NULL;
      MD = NULL;
}
Read::Read(const Read &obj){
      length = obj.length;
      besthit = obj.besthit;
      id = obj.id;
      seed_pos = obj.seed_pos;
      orientation = obj.orientation;
      score = obj.score;
      hits = obj.hits;
      sequence = obj.sequence;
      mapped = obj.mapped;
      CIGAR_length = obj.CIGAR_length;
      MD_length = obj.MD_length;
      MD = obj.MD;
      CIGAR = obj.CIGAR;
      /*for(int i=0; i<length;i++){
        sequence[i] = obj.sequence[i];
      }*/
}

Read::~Read(){

}

bool Read::direction(void){
    if(orientation == 'F' || orientation=='C'){
        return 0;
    }
    else{
        return 1;
    }
}

void Read::print(void){

    cout<<"Read number: "<<id<<endl;

    if(mapped && CIGAR!=NULL){
		cout<<"Number of hits: "<<hits<<endl;
		cout<<"Best hit: "<<besthit<<endl;
		cout<<"Score: "<<score<<endl;
		cout<<"CIGAR: "<<CIGAR<<endl;
		cout<<"MD: "<<MD<<endl;
    }else{
    	cout<<"Read not mapped."<<endl;
    }
    cout<<endl<<endl;

}

char bestReadSeed(char *quality, int length){

  int best_score = 0;
  int index;
  int score = 0;
  for(int j=0;j<length/SEEDSIZE;j++){
      score+=quality[j];
  }

  for(int i=0; i<length-length/SEEDSIZE+1; i++){
    if(score>=best_score){
      best_score = score;
      index = i;
    }
    score -= quality[i];
    score += quality[i+length/SEEDSIZE];
  }

  return index;
}

bool generate_index_file(const char *index_file_name){

	/***Create temporary file from FASTA without line breaks and headers***/

    ifstream filehandle_ref;
    filehandle_ref.open(index_file_name);
    if(!filehandle_ref.is_open()){
    	cout<<"Could not open reference file."<<endl<<"Index file was not generated."<<endl;
    	return 1;
    }
    char *reference_line = new char[MAX_LINE_SIZE];
    ofstream filehandle_new;
    filehandle_new.open(TEMP_FILE);

    while(!filehandle_ref.eof()){
    	filehandle_ref.getline(reference_line,200);
    	int line_size = filehandle_ref.gcount();
    	if(reference_line[0]!='>')
    		filehandle_new.write(reference_line,line_size-1);
    }
    filehandle_ref.close();
    filehandle_new.close();


	/***************Build FM-index instance****************/

	string index_suffix = ".fm9";
	string index_file   = string(index_file_name)+index_suffix;
	index_type fm_index;
	cout<<"Building index file..."<<endl;
	construct(fm_index,TEMP_FILE , 1); // generate index
	store_to_file(fm_index, index_file); // save it
	remove(TEMP_FILE);
	cout<<"Index file created."<<endl;

	return 0;
}

#ifdef IMPLEMENT_CUDA
void compress_seq(char *dst, const char * src, int len){

    int i = 0;
    while(i<len){
        dst[i/4] = 0;
        for(int j=0;j<4;j++){
            switch(src[i+j]){
            case 'A':
                dst[i/4] += A_VALUE<<(j*2);
                break;
            case 'T':
                dst[i/4] += T_VALUE<<(j*2);
                break;
            case 'G':
                dst[i/4] += G_VALUE<<(j*2);
                break;
            case 'C':
                dst[i/4] += C_VALUE<<(j*2);
                break;
            case 'N':
                dst[i/4] += N_VALUE<<(j*2);
                break;
            }
        }
        i+=4;
    }
}

void decompress_seq(char *dst, char *src, int len){
    for(int i=0;i<len/4 + 1;i++){
        for(int j=0;j<4;j++){
            switch((src[i]>>(j*2)) & 0x03){
            case A_VALUE:
                dst[4*i+j] = 'A';
                break;
            case C_VALUE:
                dst[4*i+j] = 'C';
                break;
            case T_VALUE:
                dst[4*i+j] = 'T';
                break;
            case G_VALUE:
                dst[4*i+j] = 'G';
                break;

            }
        }
    }
}

#endif


bool generate_sam_seqs(Read &read,unsigned char *report_nuc, unsigned int *report_pos, int report_counter){
	ostringstream cigar;
	ostringstream md;

	int ins = 0;
	int dels = 0;
	int MDmatches = 0;
	int CIGARmatches = 0;
	if(report_counter>0){
		for(int i = 0; i<report_counter; i++){
			int pos_diff;
			if(i == 0){
				pos_diff = report_pos[0]+1;
			}else{
				pos_diff = report_pos[i]-report_pos[i-1];
			}
			if(report_nuc[i]<'Z'){		//report is not a deletion
				if(report_nuc[i] == '+'){	//report is an insertion
					MDmatches++;
					if( !((pos_diff == 1 || i == 0) && report_nuc[i-1] == '+')){	//insertion not contiguous
						CIGARmatches += pos_diff-1;
						MDmatches += pos_diff - 1;
						cigar<<CIGARmatches<<'M';
						CIGARmatches = 0;
						read.score -= PGAP_REF;
					}else{
						read.score -= PGAPEXT_REF;
					}
					ins++;

				}
				else{	//report is a mismatch
					read.score -= PMISMATCH;
					if(ins){	//there are pending insertions to be written
						cigar<<ins<<'I';
						ins = 0;
					}
					if(dels){
						cigar<<dels<<'D';
						dels = 0;
					}
					if(report_nuc[i-1]>'Z')
						MDmatches++;
					MDmatches +=pos_diff-1;
					md<< MDmatches << (report_nuc[i]);
					MDmatches = 0;
					if(report_nuc[i-1]>'Z' && i!=0)  //if previous report is a deletion, add 1 to the difference
						CIGARmatches += pos_diff+1;
					else
						CIGARmatches += pos_diff;
				}
			}else{
				if( !((pos_diff == 0 || i == 0) && report_nuc[i-1] > 'Z')){ //not contiguous deletion
					CIGARmatches += pos_diff-1;
					MDmatches += pos_diff - 1;
					md<<MDmatches<<'^'<<char(report_nuc[i] - '-');
					MDmatches = 0;
					cigar<<CIGARmatches<<'M';
					CIGARmatches = 0;
					read.score -= PGAP_READ;
				}else{
					md<<char(report_nuc[i] - '-');
					read.score -= PGAPEXT_READ;
				}
				dels++;
			}
		}
		if(report_nuc[report_counter-1]){
			if(report_nuc[report_counter-1]>'Z')
				cigar<<read.length - report_pos[report_counter-1] + CIGARmatches<<'M'; //compensate last deletion discount (plus 1)
			else
				cigar<<read.length - 1 - report_pos[report_counter-1] + CIGARmatches<<'M';
			if(read.length -1 - report_pos[report_counter-1] > 0)
				md<<read.length - 1 - report_pos[report_counter-1];
		}
		if(ins){
			cigar<<ins<<'I';
		}
		if(dels){
			cigar<<dels<<'D';
		}

		md<<'\0';
		cigar<<'\0';

		read.CIGAR_length = cigar.str().length();
		read.CIGAR = new char[read.CIGAR_length];
		strcpy(read.CIGAR,cigar.str().c_str());

		read.MD_length = md.str().length();
		read.MD = new char[read.MD_length];
		strcpy(read.MD,md.str().c_str());

	}else{
		if(report_counter == 0){
			cigar<<read.length<<'M'<<'\0';
			read.CIGAR_length = cigar.str().length();
			read.CIGAR = new char[read.CIGAR_length];
			strcpy(read.CIGAR,cigar.str().c_str());

			md<<read.length<<'\0';
			read.MD_length = md.str().length();
			read.MD = new char[read.MD_length];
			strcpy(read.MD,md.str().c_str());
		}
	}
	return 0;
}


int aligner(const string &index_filename, const char *fasta_filename, const char *fastq_filename, vector<Read> *reads, int MAXREADS){

	auto start_main= chrono::high_resolution_clock::now();

	ofstream file_scores;
	file_scores.open("Scores.txt");
	int read_length;
	char *seq_storage;
	int seq_offset = 0;

	int *hit_storage;
	int hit_offset = 0;

	bool *dirs;

	index_type fm_index;

	/************Load index from file********************/
	if (!load_from_file(fm_index, index_filename)) {
	  cout<<"Could not load index from file "<<index_filename<<endl;
	  return 1;
	}
	cout << "Index loaded successfully." << endl;

//	/************Load entire reference to RAM***********/
	cout<<"Loading reference from fasta file..."<<endl;
	long ref_size = fm_index.size();
	char *reference_line = new char[MAX_LINE_SIZE];
	char *reference = new char[ref_size];
	ifstream filehandle_ref;
	filehandle_ref.open(fasta_filename);
	long  ref_load_offset = 0;
	while(!filehandle_ref.eof()){
		filehandle_ref.getline(reference_line,200);
		int line_size = filehandle_ref.gcount();
		if(reference_line[0]!='>' && line_size!=0){
			memcpy(reference+ref_load_offset,reference_line,line_size-1);
			ref_load_offset+=(line_size-1);
		}
	}
	Read::reference = reference;
	cout<<"Reference loaded."<<endl;

/**************Open reads file and initialize input buffer***/
	ifstream filehandle_reads;
	filehandle_reads.open(fastq_filename);
	char *input;
	char* name_buffer;
	input = new char[200];
	name_buffer = new char[200];
	int mapped_reads = 0;

/*******Set transformations**********************************/

	vector<Transform*> transformations;
	Straight straight;
	transformations.push_back(&straight);
	ReverseComp reversecomp;
	transformations.push_back(&reversecomp);
	Reverse reverse;
	transformations.push_back(&reverse);
	Complement complement;
	transformations.push_back(&complement);


/*****Get read length and allocate memory before loop*******/

	filehandle_reads.getline(name_buffer,MAXREADSIZE+1);
	filehandle_reads.getline(input,MAXREADSIZE+1);
	read_length = filehandle_reads.gcount()-1;
	filehandle_reads.seekg( 0 );


/****Allocate memory to store read sequences***************/
#ifdef IMPLEMENT_CUDA
	int compressed_length = (read_length + 3) / 4;
	seq_storage = new char[MAXREADS*compressed_length];
#else
	seq_storage = new char[MAXREADS*long(read_length)];
#endif
	seq_offset = 0;
	hit_storage = new int[MAXREADS];
	hit_offset = 0;
	dirs = new bool[MAXREADS];


/***Initialize temporary buffers**************************/
	char *seed = new char[read_length/SEEDSIZE];;
	char *transform_buff = new char[read_length];
	char *sequence_buffer = new char[read_length];
	int no_reads;

	/***TIMING***/
	auto start_seeds = chrono::high_resolution_clock::now();
	for(no_reads = 0; no_reads < MAXREADS; no_reads++){ //for each read in reads file

		/****Get read name sequence*****/
		filehandle_reads.getline(name_buffer,MAXREADSIZE+1);
		if(filehandle_reads.eof())
			break;

		/****Get read base sequence and construct object*****/
#ifdef IMPLEMENT_CUDA
		filehandle_reads.getline(sequence_buffer,MAXREADSIZE+1);
		read_length = filehandle_reads.gcount()-1;
		Read read(sequence_buffer,read_length);
#else
		filehandle_reads.getline(seq_storage+seq_offset,MAXREADSIZE+1);
		read_length = filehandle_reads.gcount()-1;
		Read read(seq_storage+seq_offset,read_length);
#endif
		filehandle_reads.ignore(1000,'\n'); //Ignore + sign line

		/*****Extract best seed*****************************/
		filehandle_reads.get(input,MAXREADSIZE+1);  //get quality scores
		read.seed_pos = bestReadSeed(input, read_length);
		int seed_length = read_length/SEEDSIZE;

		/****Loop for searching every transformation*******/
		char transform_seed_pos = 0;
		for(unsigned int t = 0; t < transformations.size(); t++){

			/***Generate read transformation and corresponding seed position in sequence****/
			memcpy(transform_buff, read.sequence, read.length);
			transform_seed_pos = read.seed_pos;
			transformations[t]->transform(transform_buff,read.length);
			transform_seed_pos = transformations[t]->tranSeedPos(read.seed_pos,read.length,seed_length); //transfrom position of seed according to transformed read
			memcpy(seed,transform_buff+transform_seed_pos,seed_length);
			read.orientation = transformations[t]->type();
			auto locations = locate(fm_index,string(seed,seed_length));

			/*******In case of successful mapping to reference*****/
			if(locations.size()!=0){
				read.hits = locations.size();
				mapped_reads++;

#ifdef IMPLEMENT_CUDA
				compress_seq(seq_storage + seq_offset, transform_buff, read_length);
				seq_offset += compressed_length;  //save definitive read sequence
#else
				memcpy(read.sequence,transform_buff , read.length);  //save definitive read sequence
				seq_offset += read_length;
#endif


				read.seed_pos = transform_seed_pos;		//save definitive seed position
				#ifdef SHOWCOMP
				cout<<"List of locations for seed of "<< no_reads + 1 << "th read: ";
				switch(read.orientation){
				  case 'F':
					cout<<"STRAIGHT";
					break;
				  case 'R':
					cout<<"REVERSE";
					break;
				  case 'C':
					cout<<"COMPLEMENT";
					break;
				  case 'E':
					cout<<"REVCOMP";
					break;
				}
				cout<<endl;
				#endif

				/***Select best hit of the seed between resulting list*******/

				int best_score = read.length;	//initialize variable with worst possible score
				for(int i=0; i<read.hits;i++){   //for each hit of seed in reference
					int current_score = 0;
					for(int j = 0; j<read_length; j++){      //compare every position of read with reference
						char nuc_base_ref = reference[locations[i]-read.seed_pos+j];
						char nuc_base_read = transform_buff[j];
						bool score_bit =!(( nuc_base_ref == nuc_base_read));
						current_score+= score_bit;
						#ifdef SHOWCOMP
						if(j>=transform_seed_pos && j<transform_seed_pos+seed_length)
						cout<<"\033[32m"<< score_bit<<"\033[0m";
						else
						cout<<"\033[37m"<< score_bit<<"\033[0m";
						#endif
					}
					if(current_score<best_score){
						best_score = current_score;
						read.besthit = locations[i];
					}
					#ifdef SHOWCOMP
					cout<<endl;
					cout.write(transform_buff, read.length);
					cout<<endl;
					cout.write(reference + locations[i]-transform_seed_pos, read_length);
					cout<<endl;
					cout<<endl;
					#endif
				}

				#ifdef SHOWMISS
				if(best_score >= ALIGN_THRESHOLD){

					cout<<endl<<"List of locations for seed of "<< no_reads + 1 << "th read: "<<endl;
					for(int j = 0; j<read_length-1; j++){      //print comparison of hit vs entire read
						char nuc_base_ref = reference[read.besthit-transform_seed_pos+j];
						char nuc_base_read = transform_buff[j];
						bool score_bit =!(( nuc_base_ref == nuc_base_read) | transform_buff[j] == 'N');
						if(j>=transform_seed_pos && j<transform_seed_pos+seed_length)
						cout<<"\033[32m"<< score_bit<<"\033[0m";
						else
						cout<<"\033[37m"<< score_bit<<"\033[0m";
					}
					cout<<endl;
					cout.write(transform_buff, read.length-1);
					cout<<endl;
					cout.write(reference + read.besthit-transform_seed_pos, read_length-1);
					cout<<endl;
					cout<<"Hit: "<<read.besthit<<endl;
					cout<<"Seed_pos: "<<read.seed_pos<<endl;
					cout<<"Seed length: "<<seed_length<<endl;

					incorrect_maps++;
				}
				#endif

				/***Store matching read in vector*****/
				read.mapped = true;
				read.id = no_reads;
				reads->push_back(read);
				hit_storage[hit_offset] = read.besthit - read.seed_pos;
				dirs[hit_offset] = read.direction();
				hit_offset++;
				break; //Break transformations loop
			}

		}//End of transformations loop

		#ifdef SHOWLOC
		cout<<"Seed position: "<<read.seed_pos<<endl;
		cout<<"Best hit: "<<read.besthit<<endl<<endl;
		#endif

		filehandle_reads.ignore(1000,'\n'); //Ignore read header

		//reads->push_back(read);

	} //End of reads loop

	filehandle_reads.close();
	delete seed;
	delete transform_buff;	//free temporary buffers

	/***TIMING***/
	auto stop_seeds = chrono::high_resolution_clock::now();
	long long microseconds_seeds = chrono::duration_cast<std::chrono::microseconds>(stop_seeds-start_seeds).count();

	cout<<"Total number of reads = "<<no_reads<<endl;
	cout<<"Mapped reads = "<<mapped_reads<<endl;



    /***Allocation of arrays for extend results******************************/
    unsigned char * reports = new unsigned char[long(mapped_reads)*read_length/MAXREPORTSIZE];
    unsigned int * reports_pos = new unsigned int[long(mapped_reads)*read_length/MAXREPORTSIZE];
    int * report_lengths = new int[mapped_reads];


    /***TIMING***/
    auto start = chrono::high_resolution_clock::now();




    /****Invocation of extend function**************************************/
#ifdef IMPLEMENT_CUDA
    extend_cuda(reference, ref_size, seq_storage, mapped_reads, read_length, hit_storage, dirs, reports, reports_pos, report_lengths);
#elif defined IMPLEMENT_FPGA

	Serial serial(2*(read_length/MAXREPORTSIZE)+5,2);
	string response;
    char byte_send;
    unsigned int time_fpga;
    cout<<endl<<"FPGA Responses:"<<endl;
    for(int i=3;i<reads->size();i++){
    	Read &current_read = reads->operator[](i);
    	byte_send = current_read.length;
    	serial.serial_write(&byte_send,1);
    	byte_send = current_read.direction();
    	serial.serial_write(&byte_send,1);
    	serial.serial_write(current_read.sequence,current_read.length);

    	if(current_read.direction()==0){
    		serial.serial_write(reference+current_read.besthit-current_read.seed_pos,current_read.length+ALLOWED_GAPS);
    	}
    	else{
    		serial.serial_write(reference+(current_read.besthit-current_read.seed_pos-ALLOWED_GAPS),current_read.length+ALLOWED_GAPS);
    	}
    	//usleep(1000000);

    	response = serial.serial_read();

    	report_lengths[i] = response[0];
    	time_fpga = response[1]+(response[2]<<8)+(response[3]<<16)+(response[4]<<24);
    	cout<<endl<<"Read "<<i<<": "<<int(response[0])<<endl;
    	cout<<"Time: "<<time_fpga*10<<"ns"<<endl;
    	for(char j=0;j<response[0];j++){
    		reports[long(i)*(read_length/MAXREPORTSIZE) + j] = response[j+5];
    		reports_pos[long(i)*(read_length/MAXREPORTSIZE) + j] = response[j+5+response[0]];
    		cout<<response[j+5]<<" "<<int(response[j+5+response[0]])<<endl;
    	}

    }

#else
    for(int i=0;i<reads->size();i++){
            Read &current_read = reads->operator[](i);
            report_lengths[i] = extend(current_read.length, current_read.besthit, read_length/SEEDSIZE, current_read.seed_pos, current_read.direction(), current_read.sequence, reference, reports + long(i)*(read_length/MAXREPORTSIZE), reports_pos + long(i)*(read_length/MAXREPORTSIZE));
    }
#endif
    /***TIMING***/
    auto stop = chrono::high_resolution_clock::now();
    long long microseconds_extend = chrono::duration_cast<std::chrono::microseconds>(stop-start).count();
    auto start_scoring = chrono::high_resolution_clock::now();



    /****Calculate score for each read**************************************/
    unsigned int report_offset = 0;
    unsigned int total_score = 0;
#ifdef SHOWALIGN
    cout<<"***Printing results***"<<endl;
#endif
    for(unsigned int i=0;i<reads->size();i++){
    	Read &current_read = reads->operator[](i);
    	if(current_read.mapped){
    		generate_sam_seqs(current_read,reports+report_offset*(read_length/MAXREPORTSIZE),reports_pos+report_offset*(read_length/MAXREPORTSIZE),*(report_lengths+report_offset));
    		if(*(report_lengths+report_offset)==-1)
    			mapped_reads--;

    		total_score += current_read.score;
#ifdef SHOWALIGN

			cout<<"Read "<<current_read.id<<": "<<*(report_lengths+report_offset)<<" report size"<<endl;
			if(*(report_lengths+report_offset)>0)
				cout.write((char*)(reports+report_offset*(read_length/MAXREPORTSIZE)),*(report_lengths+report_offset));
			cout<<endl;
			for(int j=0;j<*(report_lengths+report_offset);j++){
				cout<<*(reports_pos+report_offset*(read_length/MAXREPORTSIZE)+j)<<" ";
			}
			cout<<endl;
#endif
    	    report_offset++;

    	    file_scores<<current_read.id<<'\t'<<current_read.score<<'\t';
    	    if(current_read.CIGAR_length)
    	    	file_scores.write(current_read.CIGAR,current_read.CIGAR_length-1);
    	    else
    	    	file_scores<<"0";
    	    file_scores<<endl;
    	}

    }
    file_scores.close();
    total_score /= mapped_reads;

    /***TIMING***/
    auto stop_scoring = chrono::high_resolution_clock::now();
    long long microseconds_scoring = chrono::duration_cast<std::chrono::microseconds>(stop_scoring-start_scoring).count();


    /*******Print results and free remaining memory*************************************/
    cout<<endl;

    cout<<"Reads vector size: "<<reads->size()<<endl;
    cout<<"**After extend**"<<endl;
    cout<<"Mapped reads = "<<mapped_reads<<endl;
    cout<<"Average score = "<<float(total_score)/mapped_reads<<endl;
    cout<<"Time for seed search: "<<microseconds_seeds/1000000.0<<endl;
    cout<<"Time for extend: "<<microseconds_extend/1000000.0<<endl;
    cout<<"Time for SAM seqs: "<<microseconds_scoring/1000000.0<<endl;
    delete reference;
    delete input;
    auto stop_main = chrono::high_resolution_clock::now();
    long long microseconds = chrono::duration_cast<std::chrono::microseconds>(stop_main-start_main).count();
    cout<<"Total time: "<<microseconds/1000000.0<<endl;

    return mapped_reads;

}

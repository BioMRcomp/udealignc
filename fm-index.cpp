#include <vector>
#include <iostream>
#include <algorithm>
#include <iomanip>
#include <chrono>
#include <boost/program_options.hpp>
#include "transform.hpp"
#include "aligner.h"
#include "serial.h"

using namespace sdsl;
using namespace std;
namespace po = boost::program_options;



int main(int argc, char* argv[]){

	string refFile;
	string inputFile;
	bool genIndex = false;
	bool printSAM =  false;
	int MAXREADS = 100;

	po::options_description desc("Allowed options");
	desc.add_options()
	    ("input,i", po::value<string>(),"FASTQ input file")
	    ("reference,r", po::value<string>(), "FASTA reference file")
	    ("total_reads,n", po::value<int>(), "Number of reads to align from input")
	    ("generate_index,g", "Generate index file")
	    ("print_SAM,p", "Print SAM values for each read");

	po::variables_map vm;
	try{
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
	}catch(const std::exception& e){
		cout<<"Invalid parameters"<<endl;
		cout<<desc<<endl;
		return 1;
	}

	if(!vm.count("input")){
		cout<<"ERROR: Please specify input FASTQ file"<<endl;
		cout<<desc<<endl;
		return 1;
	}else{
		inputFile = vm["input"].as<string>();
	}

	if(!vm.count("reference")){
		cout<<"ERROR: Please specify reference FASTA file"<<endl;
		cout<<desc<<endl;
		return 1;
	}else{
		refFile = vm["reference"].as<string>();
	}

	if(!vm.count("total_reads")){
			cout<<"Total reads not specified. Will align 100 reads from input"<<endl;
	}else{
		MAXREADS = vm["total_reads"].as<int>();
	}

	if(vm.count("generate_index")){
		cout<<"Generating index file from reference..."<<endl;
		genIndex = true;
	}

	if(vm.count("print_SAM")){
		printSAM = true;
	}

	if(genIndex)
		generate_index_file(refFile.data());

	vector<Read> *reads = new vector<Read>();
	int mapped_reads = aligner(refFile + ".fm9", refFile.data(), inputFile.data(), reads, MAXREADS);

	if(printSAM){
		for(unsigned int i=0; i<mapped_reads; i++){
			Read &current_read = reads->operator[](i);
			current_read.print();
		}
	}

}





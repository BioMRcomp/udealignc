/*
 * extend.h
 *
 *  Created on: Apr 24, 2017
 *      Author: jlotero
 */

#ifndef EXTEND_H_
#define EXTEND_H_

#define MAXREPORTSIZE 3

int extend(int query_size, int hit, int seedsize, int seed_pos, bool dir, char* query, char* reference, unsigned char* report_nuc, unsigned int *report_pos);



#endif /* EXTEND_H_ */


#include "extendCUDA.cuh"
#include <stdlib.h>
#include <iostream>
#include <chrono>

#define A_VALUE 0b00
#define C_VALUE 0b01
#define T_VALUE 0b10
#define G_VALUE 0b11



#define CONTIGUOUS_MISSES 4
#define REPORT_SIZE 50
#define ALLOWED_GAPS 6
#define BLOCK_SIZE 1024
#define MAX_MEM_TRANSFER 1024
#define TIME

using namespace std;


__device__ void decompress(char *dst, char *src, int len){
    int compressed_length = (len + 3) / 4;
    for(int i=0;i<compressed_length;i++){
        for(int j=0;j<4;j++){
            switch((src[i]>>(j*2)) & 0x03){
            case A_VALUE:
                dst[4*i+j] = 'A';
                break;
            case C_VALUE:
                dst[4*i+j] = 'C';
                break;
            case T_VALUE:
                dst[4*i+j] = 'T';
                break;
            case G_VALUE:
                dst[4*i+j] = 'G';
                break;
            
            }
        }
    }
}

__global__ void k_extend_cuda(int read_length, int *hits, bool *dirs, char* seq_storage, char* reference,unsigned char* report_nuc, unsigned int *report_pos, int * report_lengths, int mapped_reads){
  if((blockIdx.x*BLOCK_SIZE + threadIdx.x)<mapped_reads){
  

    int compressed_length = (read_length + 3) / 4;
    
    int hit = hits[blockIdx.x*BLOCK_SIZE + threadIdx.x];
    bool dir = dirs[blockIdx.x*BLOCK_SIZE + threadIdx.x];
    //char *query = seq_storage + (blockIdx.x*BLOCK_SIZE + threadIdx.x)*read_length;  //optimizations could be made with shared and local memory
    char query[MAXREADSIZE];
    decompress(query, seq_storage + (blockIdx.x*BLOCK_SIZE + threadIdx.x)*compressed_length,read_length);
    
    char lreference[MAXREADSIZE+ALLOWED_GAPS*2];
    memcpy(lreference, reference + hit - ALLOWED_GAPS, MAXREADSIZE+ALLOWED_GAPS*2);
    
    
    unsigned char lreport_nuc[REPORT_SIZE];
    unsigned int  lreport_pos[REPORT_SIZE];
    
    report_nuc += (blockIdx.x*BLOCK_SIZE + threadIdx.x)*(read_length/MAXREPORTSIZE);
    report_pos += (blockIdx.x*BLOCK_SIZE + threadIdx.x)*(read_length/MAXREPORTSIZE);
    

    int report_counter = 0;
    int gap_counter = 0;
    int gap_trigger = 0;
    int gap_offset = 0;
    int ref_offset = 0;
    bool gap_in_ref = 0;
    
    
    
    for(int i=0; i+ref_offset<read_length; i++){
        int iter = read_length*dir -(i+1)*dir + i*!dir;
        bool val = (query[iter + ref_offset*!dir - ref_offset*dir] != lreference[iter + ALLOWED_GAPS + !dir*gap_offset - dir*gap_offset]);
        lreport_nuc[report_counter] = lreference[iter + ALLOWED_GAPS + !dir*gap_offset - dir*gap_offset];
        lreport_pos[report_counter] = i + ref_offset;
        report_counter += val;
        
        
        /*****DETECT GAP***************************************************/
        gap_trigger *= val;
        gap_trigger += val;
        
        
        
        /****REWIND LOOP AND REPORT GAP IN READ*****************************/
        i -= (gap_trigger/CONTIGUOUS_MISSES)*(CONTIGUOUS_MISSES);
        report_counter -= (gap_trigger/CONTIGUOUS_MISSES)*(CONTIGUOUS_MISSES);
        lreport_nuc[report_counter] = '-'*!gap_in_ref + '+'*gap_in_ref;
        lreport_pos[report_counter] = i + 1 + ref_offset;
        report_counter += (gap_trigger/CONTIGUOUS_MISSES);
        gap_offset += (gap_trigger/CONTIGUOUS_MISSES)*!gap_in_ref;
        ref_offset += (gap_trigger/CONTIGUOUS_MISSES)*gap_in_ref;
        
        gap_counter += gap_trigger/CONTIGUOUS_MISSES;
        
        
        /****RESET LOOP TO INCLUDE GAPS IN REF**/
        report_counter -= (gap_counter/ALLOWED_GAPS)*(report_counter+1*gap_in_ref);
        i -= (gap_counter/ALLOWED_GAPS)*i;
        i += read_length*gap_in_ref*(gap_counter/ALLOWED_GAPS); //Exit loop when maximum number of gaps exceeded
        gap_offset -= gap_offset*(gap_counter/ALLOWED_GAPS);
        gap_in_ref += gap_counter/ALLOWED_GAPS;
        gap_counter = gap_counter%ALLOWED_GAPS;
        
        
        /****RESET GAP TRIGGER*********************************************/
        gap_trigger = gap_trigger%CONTIGUOUS_MISSES;
        
        /****EXIT LOOP WHEN REPORT SIZE EXCEEDED*****************************/
        bool exit_loop = report_counter/(REPORT_SIZE-ALLOWED_GAPS-1);
        report_counter -= (report_counter+1)*exit_loop;
        i += exit_loop*read_length;
    }
    
    memcpy(report_nuc,lreport_nuc,report_counter*(report_counter>0 & report_counter < REPORT_SIZE));
    memcpy(report_pos,lreport_pos,report_counter*(report_counter>0 & report_counter < REPORT_SIZE)*sizeof(unsigned int));
    report_lengths[blockIdx.x*BLOCK_SIZE + threadIdx.x] = report_counter;
  }
}

void extend_cuda(char *reference, int ref_size,char * seq_storage,int mapped_reads,int read_length,int *hit_storage, bool *dirs, unsigned char * reports,unsigned int *reports_pos,int * report_lengths){
    
    
    char *d_seq_storage;
    char *d_reference;
    int *d_hit_storage;
    bool *d_dirs;
    unsigned char *d_reports;
    unsigned int *d_reports_pos;
    int *d_report_lengths;
    int compressed_length = (read_length + 3) / 4;
    
    long mem_requirement = long(mapped_reads)*compressed_length + long(mapped_reads)*sizeof(int) + long(mapped_reads)*sizeof(bool) \\
                          +long(mapped_reads)*(read_length/MAXREPORTSIZE) + long(mapped_reads)*(read_length/MAXREPORTSIZE)*sizeof(unsigned int) \\
                          +long(mapped_reads)*sizeof(int);
    
    //cout<<"***Memory transference***"<<endl;
    //cout<<"Mem requirement = "<<mem_requirement<<endl;
    //cout<<"Ref size = "<<ref_size<<endl;
    long mem_avail = MAX_MEM_TRANSFER*1e6 - ref_size;
    //cout<<"Available mem = "<<mem_avail<<endl;
    
    int n_transfers = mem_requirement/mem_avail + 1;
    int reads_transfer = mapped_reads/n_transfers;
    int reads_transfer_offset = reads_transfer;
    //cout<<"Number of transfers = "<<n_transfers<<endl;
    //cout<<"Reads transfer = "<<reads_transfer<<endl;
    //cout<<"Size of each transfer = "<<(reads_transfer*read_length)/1000<<endl;
    
    if(cudaMalloc((void **) &d_reference, ref_size))
        cout<<"Error allocating reference"<<endl;
    if(cudaMemcpy(d_reference, reference, ref_size, cudaMemcpyHostToDevice))
        cout<<"Error copying reference"<<endl;
    
    if(cudaMalloc((void **) &d_seq_storage, long(reads_transfer)*compressed_length))
        cout<<"Error allocating seq_storage"<<endl;
    if(cudaMalloc((void **) &d_hit_storage, long(reads_transfer)*sizeof(int)))
        cout<<"Error allocating hit_storage"<<endl;
    if(cudaMalloc((void **) &d_dirs, long(reads_transfer)*sizeof(bool)))
        cout<<"Error allocating dir_storage"<<endl;
    if(cudaMalloc((void **) &d_reports, long(reads_transfer)*(read_length/MAXREPORTSIZE)))
        cout<<"Error allocating reports"<<endl;
    if(cudaMalloc((void **) &d_reports_pos, long(reads_transfer)*(read_length/MAXREPORTSIZE)*sizeof(unsigned int)))
        cout<<"Error allocating report positions"<<endl;
    if(cudaMalloc((void **) &d_report_lengths, long(reads_transfer)*sizeof(int)))
        cout<<"Error allocating report lengths"<<endl;
    
    
    long long microseconds_transfer_one = 0;
    long long microseconds_transfer_two = 0;
    long long microseconds_ker = 0;
    for(int i = 0;i<n_transfers;i++){
    
        
        #ifdef TIME
        auto start_transfer = chrono::high_resolution_clock::now();
        #endif
        
        cudaMemcpy(d_seq_storage, seq_storage, compressed_length*long(reads_transfer), cudaMemcpyHostToDevice);
        cudaMemcpy(d_hit_storage, hit_storage, long(reads_transfer)*sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_dirs, dirs, long(reads_transfer)*sizeof(bool), cudaMemcpyHostToDevice);
        
        #ifdef TIME
        cudaDeviceSynchronize();
        auto stop_transfer = chrono::high_resolution_clock::now();
        microseconds_transfer_one += chrono::duration_cast<std::chrono::microseconds>(stop_transfer-start_transfer).count();
        #endif
        
        #ifdef TIME
        auto start_ker = chrono::high_resolution_clock::now();
        #endif
        
        k_extend_cuda<<<mapped_reads/BLOCK_SIZE+1,BLOCK_SIZE>>>(read_length, d_hit_storage, d_dirs, d_seq_storage, d_reference, 
                        d_reports, d_reports_pos, d_report_lengths, reads_transfer);
        
        #ifdef TIME
        cudaDeviceSynchronize();
        auto stop_ker= chrono::high_resolution_clock::now();        
        microseconds_ker += chrono::duration_cast<std::chrono::microseconds>(stop_ker-start_ker).count();
        #endif
        
        #ifdef TIME
        start_transfer = chrono::high_resolution_clock::now();
        #endif
        
        cudaMemcpy(reports, d_reports, long(reads_transfer)*(read_length/MAXREPORTSIZE), cudaMemcpyDeviceToHost); \\
        cudaMemcpy(reports_pos, d_reports_pos, long(reads_transfer)*(read_length/MAXREPORTSIZE)*sizeof(int), cudaMemcpyDeviceToHost);
        cudaMemcpy(report_lengths, d_report_lengths, long(reads_transfer)*sizeof(int), cudaMemcpyDeviceToHost);
        
        #ifdef TIME
        stop_transfer = chrono::high_resolution_clock::now();
        microseconds_transfer_two += chrono::duration_cast<std::chrono::microseconds>(stop_transfer-start_transfer).count();
        #endif
        
        if(i==n_transfers - 1){
            reads_transfer = mapped_reads % n_transfers;
        }
        
        seq_storage += reads_transfer_offset*compressed_length;
        hit_storage += reads_transfer_offset;
        dirs += reads_transfer_offset;
        reports += reads_transfer_offset*(read_length/MAXREPORTSIZE);
        reports_pos += reads_transfer_offset*(read_length/MAXREPORTSIZE);
        report_lengths += reads_transfer_offset;
        
    }
    
    cout<<"Transfer one: "<<microseconds_transfer_one/1000000.0<<endl;
    cout<<"Kernel: "<<microseconds_ker/1000000.0<<endl;
    cout<<"Transfer two: "<<microseconds_transfer_two/1000000.0<<endl;
    
}

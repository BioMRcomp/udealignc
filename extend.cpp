/*

HUMAN:
List of locations for seed of 9317th read: STRAIGHT
0000000000000000000000000000000000000000000000000000100000101111101110100111000110111011110111101101001110111110110001110110
ATATTTACTGCATAGACCTCCGCAGTTTATTTAATTTTTTATAATCATTGTAACAAAAGTTAGTGTTATAATTTCATTTTGTTGCGTCATAAGTCAAGTGTTTGTTTGTGCAATAAAATATTGT
ATATTTACTGCATAGACCTCCGCAGTTTATTTAATTTTTTATAATCATTGTAGCAAAATTAGTGTTATAATTTCATTTTATTGCAGCATAAGTCAAGTTTTTGTGTGTGCAATAAAATATTGTT

List of locations for seed of 8726th read:
0000000000000000000000000000000000000000011001110101111111111010111000101111111111001111011111111110011010000110001110001110
AATTAATTCCCGAGGATTTATAAAAAAAAAAAAAAAAAAAAAGACCTGCCAAGTAGTCACACCTTGCAAAAAACAGTATGCAAAACAGTTCGACACGTAGGGATTTTTTTCATTTCATTCAGTC
AATTAATTCCCGAGGATTTATAAAAAAAAAAAAAAAAAAAAGAACTGTCAAGTAGTCACACCTTGCAAAATACAGTCTGCACAACAGTTCGACACGTATGGATTATTTTCAATTAATTTCGTCC
Seed position: 0
Best hit: 143322891

List of locations for seed of 8280th read:
0000000000000000010001000000010000000000000000000000000000000000000000000110101110111011110011111111111111010111101000111101
NATTTTCGCTCGGTACGCGTGCACTTTATAACGTCTCGTTATAACGAATCTCACTGTAGATCGTTGTCTCGATGGATATTATCGCAGTTTAAGTTTGTTTTTGCCGTAACGGAAAATTAGGAAT
AATTTTCGCTCGGTACGTGTGTACTTTATCACGTCTCGTTATAACGAATCTCACTGTAGATCGTTGTCTCGATAAAAAAATTTAAAAAAAAAAGAAAACAAAATTTTTATTCTATATTTTCGAC
Seed position: 30
Best hit: 164788841

List of locations for seed of 7173th read:
0000000000010001100000000000000000000000000000000000000000000001000000001000011111111110011111011110111110111111100110001110
TGTAAAGACTTCTCTGCAGAGCTGCCAGTTGGTGTGTTGGTTGTGAATGGAGCCATTAGGTGTTTTCTCGATGATTGTTGAACTGACTGTCGCCATCACGGGAGAATGATCAGCGGAGAGACCA
TGTAAAGACTTTTCTAAAGAGCTGCCAGTTGGTGTGTTGGTTGTGAATGGAGCCATTAGGTGTATTCTCGATAATTGAACTGACTGATGCTATTACTGGGAAGAGAGATCAGCCGAGGAGTTGA
Seed position: 20
Best hit: 238437579

List of locations for seed of 6601th read:
0000000000000000000000000000000000000000000000000000000000000000011111111011110100111111110100011000110011001101111111010110
GCCCTTTTCTCGCTGCTACCCTGTCCGCAATTAACGTTGTGTTACAACTGATGTCTGTTGTTTGCAGTCATATTCCTGGCCAGATCTCGCCAAAACTTTTGTTTCAAACGGCTGTCGCATTGTT
GCCCTTTTCTCGCTGCTACCCTGTCCGCAATTAACGTTGTGTTACAACTGATGTCTGTTGTTTGCCAGTCATATTACTGGCAAGATATCGCCAAAACTTTTGTTTCAAACGGATGTCGAATTGT
Seed position: 6
Best hit: 56904562

List of locations for seed of 5525th read:
0010111111111100011111111100110111110111111111001111101001100001100000001000010010100000000000000000000000000000000000000000
NGCACGCAATAAAGGTTGAGGTCCAAAACAAGCAAAATATCTCGCAAATCGCGAAGAGTTCGTAAATTTTGATGAAATCTAGCCGTCATTTCCTTCCCGTTGGAGATATTTCGACGTTTAAGAG
TGTAATGCTCGCCCGTTTCAACGTCGAAATAAGCCCAACAGCTCGCAAATCAAATGATCTCGTCCATTTTGACGAAAACTGGTCGTCATTTCCTTCCCGTTGGAGATATTTCGACGTTTAAGAG
Seed position: 1
Best hit: 231097857

List of locations for seed of 5208th read:
0000000000000000000000000000000000000000000011111010011111111010101101111010101100100100100111110000001110110010111111100111
TTATTATTCTCTACTGAGATTTTGATATATATATATATATATATATGCATTTCTCCTTGCAATAAAGATTTGGTATCTGTAAAATATTTTATAGGATTTTTTTTAAGATTTTATCCTGATCTCA
TTATTATTCTCTACTGAGATTTTGATATATATATATATATATATGCATTTATCCTTGCAATAAAGATTTGGTATCTGTAAAATATTTTATAGGATTTTTTTTAAGATTTTATCCTGATCTCAAG
Seed position: 3
Best hit: 175647786

List of locations for seed of 4640th read:
0100101010100010111111111011111011001111111110011101111111011110011111111101110000110000000000000000000000000000000000000000
NGAAATCTTGAGATATTGCGAGCTATTTTGCTTGTTTCGACGCTTAAACGGTAGTGCATTACATTCGCAGTATTAATAGAGTTCAACGAGCGCTTAAACGTCGAAATATCTCCAACGGGAAGGA
GAAATTATCGTGATTTGCGAGCTGTTGGGATTGTTTCGACGCAGAAACGAGCTTGCATTACATTTGCAGTGTAAAGCGGAGTGAAACGAGCGCTTAAACGTCGAAATATCTCCAACGGGAAGGA
Seed position: 0
Best hit: 1301466

List of locations for seed of 3978th read:
0000000000000000000000000000000000000000000000000000000001011011000101110101011000110110011111011111111110111111011010011111
AGATAGAATAATTAAGGGAGTGCAATAGAATAGAGTGGAAAAGAGGGGAGGTAGACAGATAAGAAGAGAAATAGACATAAGAGATGTAAAACCGAAAGTTCGTCTAAAGCCGAAAGGCACGGAC
AGATAGAATAATTAAGGGAGTGCAATAGAATAGAGTGGAAAAGAGGGGAGGTAGACATAAGAAGAGAAATGGACATAAGAGATGTAAAACCGAAAGTTCGTCCAAAGCCGAAACGGACAGAACT
Seed position: 3
Best hit: 135393181

List of locations for seed of 1901th read:
0111111110011110111111110111010011111100100101110100001000000010001000000000101100101000000000000000000000000000000000000000
NTACACTGCAAATGTACTGCACGACCGTTTAAGCGTCGAAAAAACAAAATAGCTGGCAAATCAAGAAGATTTCGTCTATTTTGAAGAAAACTAACTGTCATTTCCTTCCCGTTGGAGATATTTC
TGCATGCCAAATGCAATGCACGCCCGTGTCAACGTCGAAACAAGCCCTACAGCTCGCAAATCGAGATGATTTCGTCGAGATTTACGAAAACTAACTGTCATTTCCTTCCCGTTGGAGATATTTC
Seed position: 0
Best hit: 237293621

List of locations for seed of 1800th read:
0000000000000000000000000000000000000000000001010111011000001101101111000111111011000010011001111011010110101111111111111111
TATAAATTTTTCTTTAAAAAAATTATTCGGATGTTCATCAATTTTTAATTCATTCTTTTTTATTATTATATTTTCTGCTAATAAAAATTTATTTATATTATTAATAATTATATATGTATCTATA
TATAAATTTTTCTTTAAAAAAATTATTCGGATGTTCATCAATTTTAATTCATTCTTTTTTATTATTATATTTTATGCTAATAAAAATTTATTTATATTATTAATAATTATATATGTATCTATAT
Seed position: 0
Best hit: 2438630

List of locations for seed of 700th read:
0000000000000000000000000000000000000000000000000000000010001110011111111111010100000111101111111111111110001111001101111110
ATATAATAGTAACAAGTGAAGAATCAATTTGCGAGTGGCATCGTAACTTGGCAAATAAAATCGAACAGTCTACTGAGGTTCCCCCCTCGTTGAGCGATATAGATGTTTTATCAAATAATATATG
ATATAATAGTAACAAGTGAAGAATCAATTTGCGAGTGGCATCGTAACTTGGCAAATGAAACGAAAAGTCTACTGAGGTTCCCCCCTCGTTGAGCGATATAGATGTTTTATCAAACAATATATGG
Seed position: 4
Best hit: 231735827


STAPHYLOCOCCUS:
List of locations for seed of 9782th read: STRAIGHT
00000000000000000000000011111001111111100111100100110010111111110111100011110111011111100111010001101
AGAACTTTTCAGAAAGTATTCCAAGATCTTTATGATACAAATCATTTAAATCCATTACGTAGAGGACATTTTCACGGACAATACCATTTACGGTTTTACAT
AGAACTTTTCAGAAAGTATTCCAAAGATCTTTATGATACAAATCATTTAAATCAATTACGTAGAGGACATTTTCAAGGAGAATTACATTTGCGGTTTCAAC
Best hit: 2871421
Seed position: 0

010111011111100111001111011110011101111000001000000000000000000000000000000000000-00000000000000000000
NGTTGCTACACATCCCGATTTGTACCAGTAAATGAATATATAATCATATCGCATGTCCATTTGTGCCTTTTAAATACGATGGACTCGGTGTTTTAATTCTC
GATGCTTCACATCCCGATTTGTACCTGTAAATGAATGTAATAATAATATCGCATGTCCATTTGTGCCTTTTAAATACGATGGACTCGGTGTTTTAATTCTC
Seed position: 0
Best hit: 42947

List of locations for seed of 7756th read:
01100111101010001101111001101110101110011011111101111111111101111111000000000000000000000000000000000
NTACCCGTGAAGGGGTTATTCGCGTTATTGTAATTAGTTTATTATCACAAGATCATATCGAAGTATCTATGATGCGATAATAGCCGGTAGCGGCGCTGCAA
TATCCGTGAAGGTGGTATTCGCGGTATTGTAATTAGTTTATTATCACTAGATCATATCGAAGTATCTGATGATGCGATAATAGCCGGTAGCGGCGCTGCAA
Seed position: 0
Best hit: 2618919

List of locations for seed of 7877th read:
01111101111101101111111111100111110110110110110111011010001111110010111000000000000000000000000000000
NTAGCATTGAGTAATAACTGTCAGTGTAAACGAGTTATTCTTATTGTTAGAACAATTTTACGCTAAATTACTAGCTTTCTTTTGTTCGTCAAATTTTTTCA
TATCATTGAGTAATAACTGTCAGTGTAAACGAGTTATTCTTATTGTTAGAACAATTTTACGCTAAATTACTTAGCTTTCTTTTGTTCGTCAAATTTTTTCA
Seed position: 0
Best hit: 2180894

List of locations for seed of 95th read:
00000000000000000000000000000000000000000101111111011101111110110111111110010110100101111101111000101
ATGGTCTTCCTTCATTCTCATAAAAGTTGCATCATGATCAGATCAGTTTTAGAAAAACTATTTCTATCTTTAAGAATCGTTTTTTGTTCTTCATATTTATT
ATGGTCTTCCTTCATTCTCATAAAAGTTGCATCATGATCAGTTTTAGAAAAACTATTTCTATCTTTAAGAATCGATTTTTGTTCTTCATATTTATTTTTTC
Seed position: 8
Best hit: 1522800


*/

#include <iostream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

//#define TESTING
//#define SIMPLE_INPUT
#define CONTIGUOUS_MISSES 4
#define REPORT_SIZE 50
#define ALLOWED_GAPS 6


using namespace std;



#ifdef TESTING0

ifstream::pos_type filesize(const char* filename)
{
    ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
    return in.tellg();
}


int extend(int query_size, int hit, int seedsize, int seed_pos, bool dir, char* query, char* reference, char* report_nuc, unsigned int *report_pos);

int main(int argc, char **argv){



    char report_nuc[REPORT_SIZE] = {0};
    unsigned int report_pos[REPORT_SIZE] = {0};

#ifndef SIMPLE_INPUT
    char *query = argv[2];
    int hit = atoi(argv[3]);        //Retrieve query and hit from command line
    int seed_pos = atoi(argv[4]);
    bool dir = atoi(argv[5]);
    int query_size = strlen(query);
    ifstream filehandle_ref;
    long ref_size = filesize(argv[1]);
    filehandle_ref.open(argv[1]);
    char *reference = new char[ref_size];
    filehandle_ref.get(reference,ref_size,'\0');  //Load entire reference in memory
    filehandle_ref.close();
#endif

#ifdef SIMPLE_INPUT
    char *query = argv[1];
    int query_size = strlen(query);
    char *reference = argv[2];
    bool dir = atoi(argv[3])!=0;
    int hit;
    if(dir)
    	hit = ALLOWED_GAPS;
    else
    	hit = 0;
    int seed_pos = 0;

#endif
    extend(query_size, hit, 20, seed_pos, dir, query, reference, report_nuc, report_pos);

    return 0;
}

#endif



int extend(int query_size, int hit, int seedsize, int seed_pos, bool dir, char* query, char* reference, unsigned char* report_nuc, unsigned int *report_pos){

    hit -= seed_pos;

    #ifdef TESTING

    for(int i=0; i<query_size; i++){
      if(i%10==0)
        cout<<(i/10);
      else
        cout<<" ";
    }
    cout<<endl;

    for(int i=0; i<query_size; i++){
        cout<<(i%10);
    }

    cout<<endl;

    for(int i=0;i<query_size;i++){
      cout<<(query[i]!=reference[i+hit]);
    }
    cout<<endl;

    for(int i=0;i<query_size;i++){
      cout<<(query[i]);
    }
    cout<<endl;
    for(int i=0;i<query_size;i++){
      cout<<(reference[i+hit]);
    }
    cout<<endl;

    #endif


    int report_counter = 0;
    int gap_counter = 0;
    int gap_trigger = 0;
    int gap_offset = 0;
    int ref_offset = 0;
    bool gap_in_ref = 0;

    for(int i=0; i+ref_offset<query_size; i++){
    	int iter = query_size*dir -(i+1)*dir + i*!dir;
		bool val = (query[iter + ref_offset*!dir - ref_offset*dir] != reference[iter + hit + !dir*gap_offset - dir*gap_offset]);
		report_nuc[report_counter] = reference[iter + hit + !dir*gap_offset - dir*gap_offset];
		report_pos[report_counter] = i + ref_offset;
		report_counter += val;


		/*****DETECT GAP***************************************************/
		gap_trigger *= val;
		gap_trigger += val;



		/****REWIND LOOP AND REPORT GAP IN READ*****************************/
		i -= (gap_trigger/CONTIGUOUS_MISSES)*(CONTIGUOUS_MISSES);
		report_counter -= (gap_trigger/CONTIGUOUS_MISSES)*(CONTIGUOUS_MISSES);
		//report_nuc[report_counter] = ('-' + reference[iter + hit + !dir*(gap_offset+1) - dir*(gap_offset-1)])*!gap_in_ref + '+'*gap_in_ref;
		report_nuc[report_counter] = '-'*!gap_in_ref + '+'*gap_in_ref;
		report_pos[report_counter] = i + 1 + ref_offset;
		report_counter += (gap_trigger/CONTIGUOUS_MISSES);
		gap_offset += (gap_trigger/CONTIGUOUS_MISSES)*!gap_in_ref;
		ref_offset += (gap_trigger/CONTIGUOUS_MISSES)*gap_in_ref;

		gap_counter += gap_trigger/CONTIGUOUS_MISSES;


		/****RESET LOOP TO INCLUDE GAPS IN REF**/
		report_counter -= (gap_counter/ALLOWED_GAPS)*(report_counter+1*gap_in_ref);
		i -= (gap_counter/ALLOWED_GAPS)*i;
		i += query_size*gap_in_ref*(gap_counter/ALLOWED_GAPS); //Exit loop when maximum number of gaps exceeded
		gap_offset -= gap_offset*(gap_counter/ALLOWED_GAPS);
		gap_in_ref += gap_counter/ALLOWED_GAPS;
		gap_counter = gap_counter%ALLOWED_GAPS;


		/****RESET GAP TRIGGER*********************************************/
		gap_trigger = gap_trigger%CONTIGUOUS_MISSES;

		/****EXIT LOOP WHEN REPORT SIZE EXCEEDED*****************************/
		bool exit_loop = report_counter/(REPORT_SIZE-ALLOWED_GAPS-1);
		report_counter -= (report_counter+1)*exit_loop;
		i += exit_loop*query_size;

    }

    #ifdef TESTING

    /*****DISPLAY ALIGNMENT***********************************************/
    cout<<"Report counter: "<<report_counter<<endl;

    for(int i=0; i<report_counter; i++){
         cout << report_nuc[i]<< '\t' << report_pos[i] << endl;
    }
    if(report_counter > 0){
        if(!dir){
            for(int i=0; i<query_size; i++){
              if(i%10==0)
                cout<<(i/10);
              else
                cout<<" ";
            }
            cout<<endl;

            int report_index = 0;
            for(int i=0;i<query_size;i++){

              if(i==report_pos[report_index] && report_index<=report_counter){

                if(report_nuc[report_index]=='-'){
                  cout<<"\033[32m"<<'-'<<"\033[0m";
                  i--;
                }
                else{
                  if(report_nuc[report_index]!='+')
                    cout<<"\033[32m"<<query[i]<<"\033[0m";
                }

                if(report_index <= report_counter)
                  report_index++;
              }
              else{
                cout<<query[i];
              }

            }

            cout<<endl;

            report_index = 0;
            int ref_offset = 0;
            for(int i=0; i<query_size; i++){
              if(i==report_pos[report_index] && report_index<=report_counter){
                if(report_nuc[report_index]=='+'){
                  cout<<"\033[32m"<<'+'<<"\033[0m";
                  ref_offset++;
                }
                else{
                  cout<<reference[i+hit-ref_offset];
                }

                if(report_index <= report_counter)
                  report_index++;


              }
              else{
                cout<<reference[i+hit-ref_offset];
              }
            }
            cout<<endl;
        }
        else{
            for(int i=0; i<query_size; i++){
              if(i%10==0)
                cout<<(i/10);
              else
                cout<<" ";
            }
            cout<<endl;
            int report_index = 0;
            for(int i=query_size-1;i>0;i--){

              if(i == query_size - report_pos[report_index] -1 && report_index<=report_counter){

                if(report_nuc[report_index]=='-'){
                  cout<<"\033[32m"<<'-'<<"\033[0m";
                  i++;
                }
                else{
                  if(report_nuc[report_index]=='+')
                    i++;
                  else
                    cout<<"\033[32m"<<query[i]<<"\033[0m";
                }

                if(report_index <= report_counter)
                  report_index++;
              }
              else{
                cout<<query[i];
              }

            }

            cout<<endl;

            report_index = 0;
            for(int i=0; i<query_size; i++){
              if(query_size==report_pos[report_index]+i+1){
                if(report_nuc[report_index]=='+'){
                  cout<<"\033[32m"<<'+'<<"\033[0m";
                  i--;
                }
                else{
                  cout<<reference[query_size-i-1+hit];
                }

                if(report_index <= report_counter)
                  report_index++;


              }
              else{
                cout<<reference[query_size-i-1+hit];
              }
            }
            cout<<endl;
          }
        }
    #endif

    return report_counter;


}




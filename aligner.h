/*
 * aligner.h
 *
 *  Created on: Apr 25, 2017
 *      Author: jlotero
 */

#ifndef ALIGNER_H_
#define ALIGNER_H_

#include <sdsl/suffix_arrays.hpp>
#include <string>
#include <vector>
#include <iostream>
#include <cstdio>

#define FM_DENSITY 8


#define SEEDSIZE 6
#define MAXREADSIZE 300
#define ALIGN_THRESHOLD (read_length*3)/5
#define ALLOWED_GAPS 6



/***Penalties definition***/
#define PMISMATCH 6
#define PGAP_READ 5	//Creation of gap in read (deletion)
#define PGAPEXT_READ 3 //Extension of gap in read
#define PGAP_REF 5	//Creation of gap in reference (insertion)
#define PGAPEXT_REF 3 //Extension of gap in reference

using namespace sdsl;
using namespace std;

using index_type = csa_wt<wt_huff<bit_vector,rank_support_v5<>,select_support_scan<>,select_support_scan<> >,FM_DENSITY,FM_DENSITY,text_order_sa_sampling<sd_vector<> > >;


class Read{
  public:
	Read(char *seq, int len);
	Read(const Read &obj);
	~Read();
	bool direction(void);
	void print(void);

    static char* reference;

    char *sequence;
    char *MD;
    char *CIGAR;

    int id;

    int MD_length;
    int CIGAR_length;
    int length;

    int besthit;
    int seed_pos;
    int score;
    int hits;
    char orientation;

    bool mapped;
};



bool generate_index_file(const char *index_file_name);
int aligner(const string &index_filename, const char *fasta_filename, const char *fastq_filename, vector<Read> *reads, int MAXREADS);
bool generate_sam_seqs(Read &read,unsigned char *report_nuc, unsigned int *report_pos, int report_counter);
ifstream::pos_type filesize(const char* filename);




#endif /* ALIGNER_H_ */

#include "serial.h"

Serial::Serial(int buffer_size, int wait_time){
	fd = open("/dev/ttyUSB1",O_RDWR | O_NOCTTY);
	tcgetattr(fd, &PortSettings);	/* Get the current attributes of the Serial port */

	/* Setting the Baud rate */
	cfsetispeed(&PortSettings,B9600); /* Set Read  Speed as 9600                       */
	cfsetospeed(&PortSettings,B9600); /* Set Write Speed as 9600                       */

	/* 8N1 Mode */
	PortSettings.c_cflag &= ~PARENB;   /* Disables the Parity Enable bit(PARENB),So No Parity   */
	PortSettings.c_cflag &= ~CSTOPB;   /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
	PortSettings.c_cflag &= ~CSIZE;	 /* Clears the mask for setting the data size             */
	PortSettings.c_cflag |=  CS8;      /* Set the data bits = 8                                 */

	PortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
	PortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */


	PortSettings.c_oflag &= ~OPOST;/*No Output Processing*/

	//PortSettings.c_iflag |= IGNBRK;
	/* Setting Time outs */
	PortSettings.c_cc[VMIN] = 0;
	PortSettings.c_cc[VTIME] = wait_time;

	PortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
	PortSettings.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode    */

	tcsetattr(fd,TCSANOW,&PortSettings);

	read_buffer = new char[buffer_size];
	this->buffer_size = buffer_size;
}

string Serial::serial_read(void){
	tcflush(fd, TCIFLUSH);   /* Discards old data in the rx buffer            */
	char buffer[1];   /* Buffer to store the data received              */

	int  bytes_read = 0;    /* Number of bytes read by the read() system call */
	int i = 0;
    do{
	    bytes_read = read(fd,&buffer,1); /* Read the data                   */
	    if(bytes_read){
	    	read_buffer[i] = buffer[0];
	    	i++;
	    }
	}while(!(bytes_read <= 0 || i >= buffer_size));
    //if(bytes_read == 0)
    	//cout<<"WARNING: Serial port read returned nothing."<<endl;

    read_buffer[i] = '\0';
    string received(read_buffer,i);
    return received;
}

int Serial::serial_write(char *write_buffer, int size){
	int  bytes_written  = 0;  	/* Value for storing the number of bytes written to the port */
	if(size<16)
		bytes_written = write(fd,write_buffer,size);
	else{
		int i;
		for(i=0; i<size-16;i+=16){
			bytes_written = write(fd,write_buffer+i,16);
			usleep(100000);
		}
		bytes_written = write(fd,write_buffer+i,size%16);
	}
	if(bytes_written <= 0)
		cout<<"There was a problem writing through serial"<<endl;
	return bytes_written;
}

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fcntl.h>   /* File Control Definitions           */
#include <termios.h> /* POSIX Terminal Control Definitions */
#include <unistd.h>  /* UNIX Standard Definitions 	   */
#include <errno.h>   /* ERROR Number Definitions           */
#include <string>

using namespace std;

class Serial{
public:
	Serial(int buffer_size,int wait_time);
	string serial_read(void);
	int serial_write(char *write_buffer, int size);

private:
	int fd;
	int buffer_size;
	struct termios PortSettings;
	char *read_buffer;
};

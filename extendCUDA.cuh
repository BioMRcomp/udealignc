#define MAXREPORTSIZE 3
#define MAXREADSIZE 300

void extend_cuda(char *reference, int ref_size,char * seq_storage,int mapped_reads,int read_length,int *hit_storage, bool *dirs,unsigned char * reports,unsigned int *reports_pos,int * report_lengths);

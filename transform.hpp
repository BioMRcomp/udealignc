#include <vector>



class Transform{
  public:
  virtual void transform(char*seq, int length) = 0;
  virtual int type()=0;
  //This function must return the leftmost position of the seed in the new transformation of the sequence
  virtual int tranSeedPos(int seedpos, int length, int seedsize){return seedpos;}
};

class Straight:public Transform{
  public:
  void transform(char* seq, int length){
    for(int i=0;i<length;i++)
        if(seq[i]=='N')
            seq[i]='A';
  }
  int type(){return 'F';}
};

class Complement:public Transform{
  public:
  void transform(char *seq, int length){
    for(int i=0; i<length;i++){
        switch(seq[i]){
          case 'A':
            seq[i] = 'T';
            break;
          case 'T':
            seq[i] = 'A';
            break;
          case 'G':
            seq[i] = 'C';
            break;
          case 'C':
            seq[i] = 'G';
            break;
          default:
            seq[i] = 'A';
        }
    }
  }
  int type(){return 'C';}

};

class Reverse:public Transform{
  public:
  void transform(char *seq, int length){
    for(int i=0; i<length/2; i++){
      char temp = seq[i];
      seq[i] = seq[length-1-i];
      seq[length-1-i] = temp;
    }
  }
  int type(){return 'R';}
  int tranSeedPos(int seedpos, int length, int seedsize){
    return length - seedpos - seedsize;
  }
};


class ReverseComp:public Transform{
  public:
  void transform(char *seq, int length){
    for(int i=0; i<length;i++){
        switch(seq[i]){
          case 'A':
            seq[i] = 'T';
            break;
          case 'T':
            seq[i] = 'A';
            break;
          case 'G':
            seq[i] = 'C';
            break;
          case 'C':
            seq[i] = 'G';
            break;
          default:
            seq[i] = 'A';
        }
    }
    for(int i=0; i<length/2; i++){
      char temp = seq[i];
      seq[i] = seq[length-1-i];
      seq[length-1-i] = temp;
    }
  }
  int type(){return 'E';}
  int tranSeedPos(int seedpos, int length, int seedsize){
    return length - seedpos - seedsize;
  }
};




